#include "orx.h"

orxOBJECT *shipObject;

/** Run callback every clock cycle 
 */
orxSTATUS orxFASTCALL Run()
{
  orxSTATUS eResult = orxSTATUS_SUCCESS;

  if(orxInput_IsActive("Quit")){
    eResult = orxSTATUS_FAILURE;
  }

  return eResult;
}


void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
  if (shipObject) {
	
	orxVECTOR speed = {500, 0, 0};
	orxVECTOR left_speed = {-500, 0, 0};
	
	  
    if (orxInput_IsActive("GoLeft") ) {
      orxObject_SetSpeed(shipObject, &left_speed);
	  
    }

    if (orxInput_IsActive("GoRight") ) {
      orxObject_SetSpeed(shipObject, &speed);
	  //orxObject_ApplyForce(shipObject, &speed, orxNULL);
    }
	
	    if (orxInput_HasNewStatus("Fire")) {
      //orxObject_Enable(shipBulletSpawnerOwner, orxInput_IsActive("Fire"));
    }

  }
}


void orxFASTCALL Exit()
{
  orxLOG("=================== EXIT ================");
}


void CreateExplosionAtObject(orxOBJECT *object)
{
  if (object == orxNULL)
    return;

  orxVECTOR objectVector;
  orxObject_GetWorldPosition(object, &objectVector);
  objectVector.fZ = -0.1;

  orxOBJECT *explosion = orxObject_CreateFromConfig("Explosion");

  orxObject_SetPosition(explosion, &objectVector);

  /* Rotation copied so that it starts at exactly the same place as the alien */
  orxFLOAT rotation = orxObject_GetRotation(object);
  orxObject_SetRotation(explosion, rotation);
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
  if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

    if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
      orxOBJECT *pstRecipientObject, *pstSenderObject;

      /* Gets colliding objects */
      pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
      pstSenderObject = orxOBJECT(_pstEvent->hSender);

	  if (orxString_Compare(orxObject_GetName(pstSenderObject), "ShipBulletObject") == 0) {
		  CreateExplosionAtObject(pstRecipientObject);

        orxObject_SetLifeTime(pstSenderObject, 0);
        orxObject_SetLifeTime(pstRecipientObject, 0);
      }
      else if (orxString_Compare(orxObject_GetName(pstRecipientObject), "ShipBulletObject") == 0) {
        CreateExplosionAtObject(pstSenderObject);
		  orxObject_SetLifeTime(pstSenderObject, 0);
        orxObject_SetLifeTime(pstRecipientObject, 0);
      }
	  

	  
	  
	  
    }
  }

  return orxSTATUS_SUCCESS;
}

/** Inits the game
 */
orxSTATUS orxFASTCALL Init()
{
	orxViewport_CreateFromConfig("Viewport");
	orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	
	shipObject = orxObject_CreateFromConfig("ShipObject");
	
	orxObject_CreateFromConfig("Scene");


	return orxSTATUS_SUCCESS;
}


int main(int argc, char **argv)
{
  orx_Execute(argc, argv, Init, Run, Exit);
  return EXIT_SUCCESS;
}

