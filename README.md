# Bouncer Vaders #

### What is this repository for? ###

This small game serves as the basis for a tutorial series.

### How do I get set up? ###

* Clone the repo into a folder next to orx 
* Run the build/make script, or copy in orx lib and dll manually, or use premake manually for your own IDE
* Build
* Run


### Who do I talk to? ###

* Grab me on Gitter or sausage@zeta.org.au